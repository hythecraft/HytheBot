package me.buby.HytheBot.profile;

import java.util.HashMap;
import java.util.Map;

import me.buby.HytheBot.util.exceptions.PrivateProfileException;

public class ProfileStreamDeserializer {
	public static Profile desieralizeStream(String input) throws PrivateProfileException{
		return desieralizeStream(input, "~");
	}
	/*EXAMPLE
	 * {displayname=name,uuid=id}{[class=ASSASSIN,level=0,exp=100000]~[class=xxx,level=yyy,exp=zzz]}
	 */
	public static Profile desieralizeStream(String input, String delim) throws PrivateProfileException{
		if(input.contains("private")) throw new PrivateProfileException();
		try {
			Profile ret = new Profile();
		String[] blocks = input.split("@");
		{
			blocks[0] = blocks[0].replace("{", "").replace("}", "");
			Map<String,String> content = new HashMap<>();
			String[] cont = blocks[0].split(",");
			for(String c : cont)
			{
				String[] valueSet = c.split("=");
				content.put(valueSet[0], valueSet[1]);
			}
			ret.displayName = content.get("displayname");
			ret.uuid = content.get("uuid");
		}
		{
			blocks[1] = blocks[1].replace("{", "").replace("}", "");
			String[] array = blocks[1].split(delim);
			for(String str : array)
			{
				Map<String,String> content = new HashMap<>();
				str = str.replace("[", "").replace("]", "");
				String[] cont = str.split(",");
				for(String c : cont)
				{
				
					String[] valueSet = c.split("=");
					content.put(valueSet[0], valueSet[1]);
				}
				ret.profiles.add(content);
			}
		}
		return ret;
		}catch(Exception e) {
			return null;
		}
	}
}
