package me.buby.HytheBot.profile;

public enum Classes {
	
	ASSASSIN("Assassin", "/items/diamond_hoe.png"),
	SWORDSMAN("Swordsman", "/items/diamond_sword.png"),
	RANGER("Ranger", "/items/bow.png"),
	CASTER("Caster", "/items/wooden_shovel.png");
	
	public String name;
	public String classItem;
	
	Classes(String name, String classItem)
	{
		this.name = name;
		this.classItem = classItem;
	}
	
	public static Classes getClassByName(String name) {
		for(Classes cl : Classes.values())
		{
			if(cl.name.equalsIgnoreCase(name))
				return cl;
		}
		return null;
	}
}
