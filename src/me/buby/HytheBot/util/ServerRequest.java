package me.buby.HytheBot.util;

public class ServerRequest {

	public String response;
	
	public ServerRequest(String msg) {
		this("10.0.0.114", 1236, msg);
	}
	
	public ServerRequest(String IP, int port, String msg) {
		WebInterfaceClient client = new WebInterfaceClient();
		client.startConnection(IP, port);
		String send = ""; 
		send = client.sendMessage(msg);
		client.stopConnection();
		this.response = send;
	}
}
