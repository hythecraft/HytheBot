package me.buby.HytheBot;

import java.awt.Color;
import java.net.URL;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.IOUtils;

import me.buby.HytheBot.profile.Profile;
import me.buby.HytheBot.profile.ProfileStreamDeserializer;
import me.buby.HytheBot.util.ServerRequest;
import me.buby.HytheBot.util.exceptions.PrivateProfileException;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class CommandHandler extends ListenerAdapter{

    @Override
    public void onMessageReceived(MessageReceivedEvent e)
    {
		String[] args = e.getMessage().getContentRaw().split(" ");
		if(args[0].contains("!"))
		{
			switch(args[0].replace("!", "").toLowerCase())
			{
			case "ip":
				e.getChannel().sendMessage(e.getAuthor().getAsMention() + " mc.hythecraft.com").complete();
				break;
			case "site":
			case "website":
				e.getChannel().sendMessage(e.getAuthor().getAsMention() + " https://hythecraft.com").complete();
				break;
			case "apply":
				e.getChannel().sendMessage("Want to apply for a position at Hythe?").complete();
				e.getChannel().sendMessage(e.getAuthor().getAsMention() + " Heres how you can apply: https://hythecraft.com/apply").complete();
				break;
			case "profile":
				if(args.length < 2) 
				{
					e.getChannel().sendMessage("Not enough arguments").complete();
					break;
				}
				Message m = e.getChannel().sendMessage("Retrieving user data...").complete();

				try {
					
					String url = "https://api.mojang.com/users/profiles/minecraft/"+args[1];		
					@SuppressWarnings("deprecation")
					String UUIDJson = IOUtils.toString(new URL(url));
					if(UUIDJson.isEmpty())
					{e.getChannel().sendMessage("Could not find user " + args[1]).complete();
					break;}
					
					
										String response = new ServerRequest("getprofile:" + args[1]).response;
					if(response != "" && response != null) 
						listCharacters(e.getTextChannel(), ProfileStreamDeserializer.desieralizeStream(response));
					else
						e.getChannel().sendMessage("Could not find data for " + args[1]).complete();
					
					
				}catch(PrivateProfileException pe){ 
					e.getChannel().sendMessage("This account is private!").complete();
				}
				catch (Exception e1) {
					e1.printStackTrace();
					e.getChannel().sendMessage("Invalid usage!").complete();
				}
				m.delete().complete();
			break;
			}
		}
	}
    
    public String getLongUUID(String uuid) {
    	String template = UUID.randomUUID().toString();
    	String delim = "~";
    	template = template.replaceAll("[^-]", delim);
    	int index = 0 ;
    	while(template.contains(delim))
    	{
    		template = template.replaceFirst(delim, uuid.charAt(index) + "");
    		index++;
    	}
    	return template;
    }
    
    /**
     * @param channel
     * @param profile
     */
    public void listCharacters(TextChannel channel, Profile profile) {
    	if(profile == null)
    	{
    		channel.sendMessage("Could not find player data").complete();

    		return;
    	}
    	EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle(profile.displayName + "'s characters");
		builder.setColor(Color.decode("#42a7f5"));
		for(Map<String,String> set : profile.profiles)
		{
			builder.addField(set.get("class"), "Lvl. " + set.get("level"), false);
		}
		channel.sendMessage(builder.build()).complete();
		channel.sendMessage("https://hythecraft.com/profile/" + profile.displayName).complete();
    }
    
    
    public void usageMessage(TextChannel channel) {
		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle("HytheBot");
		builder.setColor(Color.decode("#42a7f5"));
		builder.setDescription("test");
		builder.addField("usage", "TEST", false);
		channel.sendMessage(builder.build()).complete();
    }
    
    
    
    
    
    
    
    
    

}
